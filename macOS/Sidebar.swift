//
//  Sidebar.swift
//  HackerNews (macOS)
//
//  Created by Cavelle Benjamin on Aug/29/20.
//

import SwiftUI

import SwiftUI

struct Sidebar: View {
  var body: some View {
    List(Category.allCases) { category in
      NavigationLink(destination: ItemsListView(viewModel: ItemsViewModel(category: category))) {
        Label(category.name, systemImage: category.icon)
      }
    }
    .listStyle(SidebarListStyle())
    .frame(minWidth: 150, idealWidth: 150, maxWidth: 200, maxHeight: .infinity)
    .padding(.top, 16)
  }
}

struct Sidebar_Previews: PreviewProvider {
    static var previews: some View {
        Sidebar()
    }
}
