//
//  ItemView.swift
//  HackerNews
//
//  Created by Cavelle Benjamin on Aug/29/20.
//

import SwiftUI

struct ItemView: View {
  @ObservedObject var viewModel: ItemViewModel
  
  var body: some View {
    if viewModel.loading {
      ProgressView().onAppear(perform: {
        viewModel.reload()
      })
    } else if let error = viewModel.error {
      Label(error.localizedDescription, systemImage: "exclamationmark.triangle")
    } else if let item = viewModel.item {
      Link(destination: item.url) {
        VStack(alignment: .leading) {
          Text(item.title)
            .font(.headline)
            .lineLimit(3)
          HStack {
            Text("\(item.score) points * by \(item.author) * \(item.formattedDate)")
              .font(.caption)
              .foregroundColor(.gray)
          }
        }
      }
    }
  }
}

struct ItemView_Previews: PreviewProvider {
    static var previews: some View {
      ItemView(viewModel: ItemViewModel(id: 24315782))
    }
}
