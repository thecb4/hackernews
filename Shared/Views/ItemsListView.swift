//
//  ItemsListView.swift
//  HackerNews
//
//  Created by Cavelle Benjamin on Aug/29/20.
//

import SwiftUI

struct ItemsListView: View {
  
  @ObservedObject var viewModel: ItemsViewModel
  
    var body: some View {
      #if os(macOS)
        return shared
          .frame(minWidth: 400, minHeight: 600)
          .toolbar {
            ToolbarItem(placement: .automatic) {
              Button(action: { viewModel.reload() }) {
                Image(systemName: "arrow.clockwise")
              }
              .keyboardShortcut("R", modifiers: .command)
            }
          }
      #else
        return shared
          .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
              Button(action: { viewModel.reload() }) {
                Image(systemName: "arrow.clockwise")
              }
            }
          }
      #endif
    }
  
  @ViewBuilder
  private var shared: some View {
    if viewModel.loading {
      ProgressView()
        .onAppear(perform: { viewModel.reload() })
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    } else if let error = viewModel.error {
      Label(error.description, systemImage: "exclamationmark.triangle")
    } else {
      List(viewModel.items) { item in
        ItemView(viewModel: ItemViewModel(id: item.id))
      }
    }
  }
}

struct ItemsListView_Previews: PreviewProvider {
    static var previews: some View {
      NavigationView {
        ItemsListView(viewModel: ItemsViewModel(category: .top))
      }
    }
}
