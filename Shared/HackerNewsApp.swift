//
//  HackerNewsApp.swift
//  Shared
//
//  Created by Cavelle Benjamin on Aug/29/20.
//

import SwiftUI

@main
struct HackerNewsApp: App {
    var body: some Scene {
        WindowGroup {
          #if os(macOS)
            NavigationView {
              Sidebar()
              ItemsListView(viewModel: ItemsViewModel(category: .top))
            }
          #else
          TabBar()
          #endif
        }
    }
}

struct HackerNewsApp_Previews: PreviewProvider {
  static var previews: some View {
    #if os(macOS)
      NavigationView {
        Sidebar()
        ItemsListView(viewModel: ItemsViewModel(category: .top))
      }
    #else
    TabBar()
    #endif
  }
}
