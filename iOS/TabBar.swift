//
//  TabBar.swift
//  HackerNews (iOS)
//
//  Created by Cavelle Benjamin on Aug/29/20.
//

import SwiftUI

import SwiftUI

struct TabBar: View {
  var body: some View {
    TabView {
      ForEach(Category.allCases) { category in
        NavigationView {
          ItemsListView(viewModel: ItemsViewModel(category: category))
            
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .tabItem {
          Image(systemName: category.icon)
          Text(category.name)
        }
      }
    }
  }
}

struct TabBar_Previews: PreviewProvider {
    static var previews: some View {
      TabBar()
    }
}
